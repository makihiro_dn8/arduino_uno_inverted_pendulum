#include <SPI.h>
#include <MsTimer2.h>
#include "CircularBuffer.h"

#define BUF_LEN 200
//typedef CircularBuffer<unsigned char, BUF_LEN> Buffer_UChar;
typedef CircularBuffer<int, BUF_LEN> Buffer_Word;
Buffer_Word buf_diff;
Buffer_Word  buf_val;

// ピン定義。
#define PIN_SPI_MOSI 11
#define PIN_SPI_MISO 12
#define PIN_SPI_SCK 13
#define PIN_SPI_SS 10
#define PIN_BUSY 9
#define PIN_SENSOR A0

#define STEP_MODE 0x02 /// 0:フルステップ, 1:1/2ステップ, 2:1/4ステップ
#define STEP_ROT 200 //１回転あたりのステップ数

//#define STEP_MODE 0x00 /// 0:フルステップ, 1:1/2ステップ, 2:1/4ステップ
//#define STEP_ROT 400 //１回転あたりのステップ数

#define ABS_POS_MAX 4194304

#define MODE_STOP 0
#define MODE_MOVE 1
#define MODE_INIPOS 2
#define MODE_F 3
#define MODE_B 4
#define MODE_END 99


int cnt = 50;
int dir = 0;
int mode = MODE_STOP;
int stepCount = 0;
//const int sensorMin = 45;
//const int sensorMax = 925;
const int sensorMin =65;
const int sensorMax = 660;
const int sensorMid = (sensorMin+sensorMax)/2;

const int moveWidth = 32*pow(2,STEP_MODE)*(STEP_ROT/200);
const int safeLimitStep = 200*pow(2,STEP_MODE)*(STEP_ROT/200)*50;

////const int moveWidth = 32*pow(2,STEP_MODE)*(STEP_ROT/200);
//const int moveWidth = pow(2,STEP_MODE)*(STEP_ROT/200);
//const int safeLimitStep = 200*pow(2,STEP_MODE)*(STEP_ROT/200)*5;

//int pos_sensor = sensorMid;
int pos_sensor  = 377; //目標センサ値
int pos_ref = 0;
bool timerFlg = false;
int g_sensor = 0;
int g_pos = 0;

int T = 5; //制御周期

// PID制御パラメータ
//double param_k_pos_p = 1.5;
//double param_k_pos_d = 0.3;
//double param_k_pos_i = 0.;
//double param_k_ang_p = 3.0;
//double param_k_ang_d = 0.06;
//double param_k_ang_i = 6.0; //まあまあ安定

double param_k_pos_p = 0.3;
double param_k_pos_d = 0.7;
double param_k_pos_i = 0.;
double param_k_ang_p = 3.0;
double param_k_ang_d = 0.15;
double param_k_ang_i = 6.0; //まあまあ安定


//// 4分成功時のパラメータ
//K_pos_p :  0.50
//K_pos_d :  0.10
//K_pos_i :  0.00
//K_ang_p :  3.00
//K_ang_d :  0.06
//K_ang_i :  6.00


//// 更に安定した感じ
//K_pos_p :  0.10
//K_pos_d :  0.10
//K_pos_i :  0.00
//K_ang_p :  3.00
//K_ang_d :  0.15
//K_ang_i :  6.00
//moveWidth : 128.00
//safeLimitStep : 32767.00
//cur pos : -256.00


//// 動きが激しいが、これも悪くない
//K_pos_p :  0.30
//K_pos_d :  0.70
//K_pos_i :  0.00
//K_ang_p :  3.00
//K_ang_d :  0.15
//K_ang_i :  6.00
//moveWidth : 128.00
//safeLimitStep : 32767.00
//cur pos : 512.00



void setup()
{
  delay(2000);
  // ピン設定。
  pinMode(PIN_SPI_MOSI, OUTPUT);
  pinMode(PIN_SPI_MISO, INPUT);
  pinMode(PIN_SPI_SCK, OUTPUT);
  pinMode(PIN_SPI_SS, OUTPUT);
  pinMode(PIN_BUSY, INPUT);
  digitalWrite(PIN_SPI_SS, HIGH);
  //シリアル通信
//  Serial.begin(9600);
  Serial.begin(38400);
  while(!Serial){;}
  //SPI通信開始
  SPI.begin();
  SPI.setDataMode(SPI_MODE3);//SCKの立ち上がりでテータを送受信＆アイドル時はpinをHIGHに設定
  SPI.setBitOrder(MSBFIRST);//MSBから送信
 
  L6470_setup();//L6470を設定 

  Serial.println("ready.");

  MsTimer2::set(T, timer_interapt);//シリアルモニター用のタイマー割り込み
  MsTimer2::start();

  dispParam();
}

int readSensor(){
  int sensorValue = analogRead(PIN_SENSOR);
  String s= String(sensorValue, DEC);
  String s2= String((978+90)/2, DEC);
//  Serial.println(s+":"+s2);
//  Serial.println(sensorValue);
  return sensorValue;
}


int serial_gets(char *buffer, int size) {
  if (!Serial.available()) return 0;
  int i = 0;
  while (i < size-1) {
    if (Serial.available()) {
      buffer[i++] = Serial.read();
      if (buffer[i-1] == '\r') { i--; }
      if (buffer[i-1] == '\n') { i--; break; }
    } else {
      delayMicroseconds(50);
    }
  }
  buffer[i] = '\0';
  return i;
}

char* D2S(double val, char *s){
  return dtostrf(val, 5, 2, s);
}

void dispParam(){
  char buf[128];
//  sprintf(buf, "params = [%lf, %lf, %lf], [%lf, %lf, %lf]", param_k_pos_p, param_k_pos_d, param_k_pos_i,param_k_ang_p,param_k_ang_d,param_k_ang_i);
  char dbuf[16];
  sprintf(buf, "K_pos_p : %s", D2S(param_k_pos_p, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "K_pos_d : %s", D2S(param_k_pos_d, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "K_pos_i : %s", D2S(param_k_pos_i, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "K_ang_p : %s", D2S(param_k_ang_p, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "K_ang_d : %s", D2S(param_k_ang_d, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "K_ang_i : %s", D2S(param_k_ang_i, dbuf));
  Serial.println(String(buf));


  sprintf(buf, "moveWidth : %s", D2S(moveWidth, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "safeLimitStep : %s", D2S(safeLimitStep, dbuf));
  Serial.println(String(buf));
  sprintf(buf, "cur pos : %s", D2S(getAbsPos(), dbuf));
  Serial.println(String(buf));
}

void setParam(String input){
  double * tarVal = 0;
  if (input.substring(0,3)=="kpp"){
    tarVal = &param_k_pos_p;
  }
  else if (input.substring(0,3)=="kpd"){
    tarVal = &param_k_pos_d;
  }
  else if (input.substring(0,3)=="kpi"){
    tarVal = &param_k_pos_i;
  }
  else if (input.substring(0,3)=="kap"){
    tarVal = &param_k_ang_p;
  }
  else if (input.substring(0,3)=="kad"){
    tarVal = &param_k_ang_d;
  }
  else if (input.substring(0,3)=="kai"){
    tarVal = &param_k_ang_i;
  }

  char buf[16];
  input.substring(3,16).toCharArray(buf, 16);
  *tarVal = atof(buf);

  dispParam();  
}

void procSerialInput(){
  char readbuf[64] = {0};
  long t1 = micros();
  int readpos = serial_gets(readbuf, sizeof(readbuf));
  long t2 = micros();
  String input = String(readbuf);

  if (readpos > 0){
    Serial.println("----");
    Serial.println(input);
    Serial.println(readpos);
    Serial.println(String(t2-t1));

    if (input=="i"){
      mode = MODE_INIPOS;
    }
    else if(input=="g"){
      Serial.println(String(getAbsPos()));
    }
    else if(input=="f"){
      mode = MODE_F;
    }
    else if(input=="b"){
      mode = MODE_B;
    }
    else if(input=="m"){
      clearBuff();
      mode = MODE_MOVE;
      init_modeMove_furiage_toritsu(); //倒立振り上げのための初期化
    }
    else if(input=="r"){
      reset();
    }
    else if(input=="c"){
      pos_sensor = readSensor();
      Serial.println("pos_sensor:" + String(pos_sensor));
    }
    else if(input=="e"){
      mode = MODE_END;
      L6470_terminate();
    }
    else if(input=="d"){
      dumpBuff();
    }
    else if(input=="info"){
      dispParam();
    }
    else if(input.substring(0,1)=="k"){
      setParam(input);
    }
    else{
      mode = MODE_STOP;
    }
  }
}

void reset(){
  stepCount = 0;
  pos_ref = g_pos;
  L6470_send(0xD8);

  mode = MODE_STOP;
  Serial.println("reset");
  Serial.println("pos_ref : " + String(pos_ref));
}
void timer_interapt()
{
  timerFlg = true;
  g_sensor = readSensor();
  g_pos = getAbsPos();
}


void clearBuff(){
  buf_diff.clear_();
  buf_val.clear_();  
}

void dumpBuff(){
  while(Buffer_Word::Empty<buf_diff.remain()){
    Serial.println(String(buf_diff.pop()) + "," + String(buf_val.pop()));
  }
}

void modeMove(){
  modeMove_furiage_toritsu();  
//  modeMove_oufuku(); 
//  modeMove_toritsu();
//  modeMove_test();
}

void modeStop(){
  sendStop();
}

void modeIniPos(){
  int val = getAbsPos();
  moveStep(abs(val), val > 0 ? 0 : 1);
  mode = MODE_STOP;
}

void moveStep(int val_step, char dir){
  dir = dir > 0 ? 1 : 0;
  for(int cnt=0; cnt<val_step; ++cnt){
    if(!sendStep(dir)) break;
    while(busy()){;}
  }
}

void modeFor(){
  Serial.println("--modeFor--");
  Serial.println(String(moveWidth));
  moveStep(moveWidth, 1);
  mode = MODE_STOP;
  Serial.println("--modeFor end--");
  dispParam();
}

void modeBack(){
  Serial.println("--modeBack--");
  Serial.println(String(moveWidth));
  moveStep(moveWidth, 0);
  mode = MODE_STOP;
  Serial.println("--modeBack end--");
  dispParam();
}

void modeEnd(){
  // do nothing
  ;
}

void loop()
{
  procSerialInput();
  readSensor();

  switch(mode){
    case MODE_STOP:
    modeStop();
    break;
    case MODE_INIPOS:
    modeIniPos();
    break;
    case MODE_F:
    modeFor();
    break;
    case MODE_B:
    modeBack();
    break;
    case MODE_END:
    modeEnd();
    break;
    case MODE_MOVE:
    modeMove();
    break;
  }
}

