#include <SPI.h>

void sendGoTo(long pos){
  L6470_transfer(0x60,3,pos);
}

void sendStop(){
  L6470_send(0xB8);//HradStop 
}

void sendStep_simple(long val, int dir){
    L6470_send(0x40+dir);//Run(DIR,SPD),0x51:正転,0x50:逆転　
    L6470_send(0x00);//SPD値(20bit)
    L6470_send(0x00);
    L6470_send(val&0xFF);  
}
 
bool sendStep(char dir){
  if (abs(getAbsPos())>safeLimitStep){
    //安全のために止める
    mode=MODE_STOP;
    Serial.println("Safety stop.");
    return false;
  }else{
    //動かす
    dir = dir > 0 ? 1 : 0;
    L6470_send(0x40+dir);//Run(DIR,SPD),0x51:正転,0x50:逆転　
    L6470_send(0x00);//SPD値(20bit)
    L6470_send(0x00);
    L6470_send(0x01);  

//    L6470_send(0x50+dir);//Run(DIR,SPD),0x51:正転,0x50:逆転　
//    L6470_send(0x00);//SPD値(20bit)
//    L6470_send(0x00);
//    L6470_send(0x01);  


    stepCount += dir==0 ? (1) : (-1);
    return true;
  }
}

bool sendRun(long val, char dir){
  long pos = getAbsPos();
  if (abs(pos)>safeLimitStep){
    //安全のために止める
    mode=MODE_STOP;
    Serial.println("Safety stop.: " + String(safeLimitStep) + ":" + String(pos));
    return false;
  }else{
    //動かす
    dir = dir > 0 ? 1 : 0;
    L6470_send(0x50+dir);//Run(DIR,SPD),0x51:正転,0x50:逆転　
    L6470_send((val&0xFF0000)>>16);//SPD値(20bit)
    L6470_send((val&0x00FF00)>>8);
    L6470_send((val&0x0000FF));
    while(busy()){;}

    stepCount += dir==0 ? (1) : (-1);
    return true;
  }
}

long getAbsPos(){
  long pos = L6470_getparam_abspos();
  while(busy()){;}
  return pos;
}

int busy(){
  return !digitalRead(PIN_BUSY);
}
void L6470_busydelay(long time){//BESYが解除されるまで待機
  while(!digitalRead(PIN_BUSY)){
  }
  delay(time);
}
 
void L6470_send(unsigned char add_or_val){
  digitalWrite(PIN_SPI_SS, LOW); 
  SPI.transfer(add_or_val); // アドレスもしくはデータ送信。
  digitalWrite(PIN_SPI_SS, HIGH); 
}

void L6470_send_u(unsigned char add_or_val){//busyを確認せず送信するため用
  digitalWrite(PIN_SPI_SS, LOW); // ~SSイネーブル。
  SPI.transfer(add_or_val); // アドレスもしくはデータ送信。
  digitalWrite(PIN_SPI_SS, HIGH); // ~SSディスエーブル。
}

long L6470_getparam(int add,int bytes){
  long val=0;
  int send_add = add | 0x20;
  L6470_send_u(send_add);
  for(int i=0;i<=bytes-1;i++){
    val = val << 8;
    digitalWrite(PIN_SPI_SS, LOW); // ~SSイネーブル。
    val = val | SPI.transfer(0x00); // アドレスもしくはデータ送信。
    digitalWrite(PIN_SPI_SS, HIGH); // ~SSディスエーブル 
  }
  return val;
}


void L6470_transfer(int add,int bytes,long val){
  int data[3];
  L6470_send(add);
  for(int i=0;i<=bytes-1;i++){
    data[i] = val & 0xff;  
    val = val >> 8;
  }
  if(bytes==3){
    L6470_send(data[2]);
  }
  if(bytes>=2){
    L6470_send(data[1]);
  }
  if(bytes>=1){
    L6470_send(data[0]);
  }  
}


long L6470_getparam_abspos(){
  long val = L6470_getparam(0x01,3);
  if (val > ABS_POS_MAX/2){
    val = val-ABS_POS_MAX;
  }
  return val;
}
 
void L6470_setup(){
  //前のコマンドの引数を消去
  L6470_send(0x00);//nop
  L6470_send(0x00);
  L6470_send(0x00);
  L6470_send(0x00);
  //デバイスリセットコマンド
  L6470_send(0xc0);//ResetRevice
  
  //最大回転スピード
  long spd = 0x0023L*(STEP_ROT/200);
  L6470_transfer(0x07, 2, spd); //10bit

  //モータ停止中の電圧設定
  L6470_send(0x09);//レジスタアドレス
  L6470_send(0xFF);//値(8bit),デフォルト0x29
  //モータ定速回転時の電圧設定
  L6470_send(0x0a);//レジスタアドレス
  L6470_send(0xFF);//値(8bit),デフォルト0x29
  //加速中の電圧設定
  L6470_send(0x0b);//レジスタアドレス
  L6470_send(0xFF);//値(8bit),デフォルト0x29
  //減速中の電圧設定
  L6470_send(0x0c);//レジスタアドレス
  L6470_send(0xFF);//値(8bit),デフォルト0x29  

  //フ ル ス テ ッ プ,ハ ー フ ス テ ッ プ,1/4, 1/8,…,1/128 ステップの設定
  L6470_send(0x16);//レジスタアドレス
//  L6470_send(0x00);//値(8bit)
  L6470_send(STEP_MODE);//値(8bit) 


//  long acc = 0x03E8L*(STEP_ROT/200); //これぐらい出て欲しい加速度
  long acc = 0x02E8L*(STEP_ROT/200); //これぐらい出て欲しい加速度
//  int acc = 0x0100L*(STEP_ROT/200);//安定したかそくど
  L6470_transfer(0x05, 2, acc);
  L6470_transfer(0x06, 2, acc);
 
}

void L6470_terminate(){
  //安全のために終了させるコマンド。負荷電圧を0にする。
  
  //前のコマンドの引数を消去
  L6470_send(0x00);//nop
  L6470_send(0x00);
  L6470_send(0x00);
  L6470_send(0x00);
  //デバイスリセットコマンド
  L6470_send(0xc0);//ResetRevice
  
  //最大回転スピード
  L6470_send(0x07);//レジスタアドレス
  L6470_send(0x00);//値(10bit),デフォルト0x41
  L6470_send(0x00);

  //モータ停止中の電圧設定
  L6470_send(0x09);//レジスタアドレス
  L6470_send(0x00);//値(8bit),デフォルト0x29
  //モータ定速回転時の電圧設定
  L6470_send(0x0a);//レジスタアドレス
  L6470_send(0x00);//値(8bit),デフォルト0x29
  //加速中の電圧設定
  L6470_send(0x0b);//レジスタアドレス
  L6470_send(0x00);//値(8bit),デフォルト0x29
  //減速中の電圧設定
  L6470_send(0x0c);//レジスタアドレス
  L6470_send(0x00);//値(8bit),デフォルト0x29  
 
}

