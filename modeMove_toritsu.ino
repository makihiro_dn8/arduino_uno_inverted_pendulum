void modeMove_toritsu(){
//  int sensor = readSensor();
  const int wind = (sensorMax-sensorMid)/2;
  int diff_0 = g_sensor-pos_sensor;
  static int diff_old = 0;
  static double i_diff = 0;
  static double i_diff_pos = 0;
  static double diff_pos_norm_old = 0;
  static unsigned long t1;
  static unsigned long t2;
  static bool go_one_flg = false;
  if (abs(diff_0) > wind){
    // 範囲外のため停止
    sendStop();
    diff_old = 0;
    i_diff = 0;
    go_one_flg = false;
    mode = MODE_STOP;
  }else{
    // フィードバック制御
    if(!busy() && timerFlg){
//      int maxspeed = 0x40;
      long maxspeed = 0x008000L * (STEP_ROT/200);

      const double k_pos_p = param_k_pos_p;
      const double k_pos_d = param_k_pos_d/T;
      const double k_pos_i = param_k_pos_i/T;
      
      int diff_pos = g_pos - pos_ref; // pos_ref is reference of step
      double diff_pos_norm = double(diff_pos)/moveWidth; //Fモードでの移動量を基準に正規化
      double d_diff_pos_norm = diff_pos_norm - diff_pos_norm_old;

      double pos_sensor_diff = diff_pos_norm*k_pos_p + i_diff_pos*k_pos_i + d_diff_pos_norm*k_pos_d;
      double pos_sensor_diff_max = 20;
      if (abs(pos_sensor_diff) > pos_sensor_diff_max){
        pos_sensor_diff = (pos_sensor_diff > 0 ? 1 : -1)*pos_sensor_diff_max;
        Serial.println("over ... ");
      }
      int diff_1 = g_sensor-(pos_sensor + pos_sensor_diff);
      
      const double kp = maxspeed*param_k_ang_p;
      const double kd = maxspeed*param_k_ang_d/T;
      const double ki = maxspeed*param_k_ang_i/T;
      
      double diff_norm = double(diff_1)/wind; //およそ-1～1になるよう正規化
      double d_diff_norm = double(diff_1-diff_old)/10*(T/20); //およそ-1～1になるよう正規化. 10というのはT=20のときの実測値より。

      long val = diff_norm*kp + d_diff_norm*kd + i_diff*ki;
      long dir = val > 0 ? 0 : 1;
      long abs_val = abs(val);
      if(abs_val>maxspeed){
        Serial.println(String(millis()) + ":"+String(abs_val)+":"+String(diff_0)+":"+String(diff_0-diff_old)+":"+String(kp)+":"+String(kd));
        abs_val = maxspeed;
      }
      if(go_one_flg){
        abs_val = 0xFF;
        Serial.println("ho");
//        sendRun((maxspeed&0xFF)<<8, 1);
        sendRun((0x70)<<8, 1);
        Serial.println("ho_end");
      }else{
//        sendRun((abs_val&0xFF)<<8, dir);
        buf_diff.push(diff_0);
        buf_val.push(int((abs_val&0xFFFF00)>>8)*(dir==1?1:-1));
        sendRun(abs_val, dir);
        diff_old = diff_0;
        i_diff += diff_norm;
        i_diff_pos += diff_pos_norm;
        diff_pos_norm_old = diff_pos_norm;
      }
      
      timerFlg = false;
    }
  }
}


