
#define FURIAGE 0
#define TORITSU 1
static int stat_furiage_toritsu = FURIAGE;

void init_modeMove_furiage_toritsu(){
  stat_furiage_toritsu = FURIAGE;
}

void modeMove_furiage_toritsu(){
  if (stat_furiage_toritsu == FURIAGE){
    modeMove_furiage();
    int range = 20;
    if (abs(pos_sensor-g_sensor)<range){
      stat_furiage_toritsu = TORITSU;
    }
  }else{
    modeMove_toritsu();
  }
}

void modeMove_furiage(){
  if (!busy() && timerFlg){
    static int dir = 1;
    long pos = g_pos/pow(2, STEP_MODE);
    if (dir==1){
//      if (pos > 15*(STEP_ROT/200)){
      if (pos > 25*(STEP_ROT/200)){
        dir = 0;
      }
    }else{
      if (pos < 0*(STEP_ROT/200)){
        dir = 1;
      }
    }
    sendRun(0x006BFC, dir);
    while(busy()){;}
    timerFlg = false;
  }
}

void modeMove_oufuku(){
  if (!busy() && timerFlg){
    static int dir = 1;
//    long pos = getAbsPos()/pow(2, STEP_MODE);
    long pos = g_pos/pow(2, STEP_MODE);
//    long pos = g_pos;
    if (dir==1){
      if (pos > 10*(STEP_ROT/200)){
        dir = 0;
      }
    }else{
      if (pos < 0*(STEP_ROT/200)){
        dir = 1;
      }
    }
//    Serial.println(String(pos) + ":" + String(dir) + ":" + String(g_pos));

//    sendRun(0x010000, dir);
    sendRun(0x008BFC, dir);
//    sendRun(0x002000L, dir);
    while(busy()){;}
    
//    sendRun(0x000BFC, dir);
//    sendRun(0x001BFC, dir);
    timerFlg = false;
  }
}

void modeMove_test(){
  const int wind = (sensorMax-sensorMid)/2;
  int diff = g_sensor-pos_sensor;
  static int diff_old = 0;
  static int i_diff = 0;
  static unsigned long t1;
  static unsigned long t2;
  static bool go_one_flg = false;
  static int dir = 0;
  if (abs(diff) > wind){
    // 範囲外のため停止
    sendStop();
    diff_old = 0;
    i_diff = 0;
    go_one_flg = false;
    dir = 0;
    mode = MODE_STOP;
  }else{
    // フィードバック制御
    if(!busy() && timerFlg){
      if (go_one_flg){
        long maxspeed = 0x008000L * (STEP_ROT/200);
        Serial.println(String(maxspeed, 16)+":"+String(STEP_ROT/200));
        sendRun(maxspeed, dir);
      }else{
        if (abs(diff) > 64*1){
          dir = diff > 0 ? 0 : 1;
          go_one_flg = true;
          Serial.println("flagOn:"+String(diff));
        }
        sendRun((0&0xFF)<<8, dir);
        diff_old = diff;
      }

      timerFlg = false;
    }
  }
}



//void ring_buf_test()
//{
//  // リングバッファテストコード
//  for (int i=0; i<1000; ++i){
//    buf.push(i);
//  }
//  while(Buffer::Empty<buf.remain()){
//    Serial.println(String(buf.pop()));
//  }
//  
//}

